<?php

use App\Model\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'username'  => 'superadmin'
        ],[
            'name'      => 'Superadmin',
            'password'  => password_hash('admin2121', PASSWORD_BCRYPT),
            'role'      => 'superadmin'
        ]);

        User::firstOrCreate([
            'username'  => 'operator'
        ],[
            'name'      => 'Operator',
            'password'  => password_hash('password', PASSWORD_BCRYPT),
            'role'      => 'operator'
        ]);
    }
}
