jQuery(document).ready(function(){
    var table = $('#dataTables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        bFilter: true,
        lengthChange: true,
        ajax: {
            url: "/gejala",
            type: 'POST',
        },
        columns: [
            {
                data: null,
                className:'control',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                "width": "20px",
                "orderable": false,
            },
            {
                data: 'code',
            },
            {
                data: 'name',
            },
            {
                width: '20px',
                data: null,
                orderable: false,
                className: 'text-right',
                render: function (data, type, row) {
                    return  "<a class='mb-2 mr-2 btn-transition btn btn-outline-primary btn-sm' href='/gejala/edit/"+data.id+"' title='Sunting'><i class='pe-7s-edit'></i></a>" +
                        "<button class='btn-delete mb-2 mr-2 btn-transition btn btn-outline-danger btn-sm' data-item='"+JSON.stringify(data)+"' title='Hapus'><i class='pe-7s-trash'></i></button>";
                }
            }
        ],
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 3 },
            { responsivePriority: 3, targets: 1 },
        ]
    });

    $('#dataTables').on('click', 'button.btn-delete', function(event){
       event.preventDefault();
       var data = $(this).data('item');
       $('#btn-delete').data('id', data.id);
       $('#modal-delete').modal('toggle');
    });

    $('#btn-delete').click(function(){
        var id = $(this).data('id');
        $.ajax({
            url : '/gejala/delete/' + id,
            method : 'POST',
            success : function(response){
                if (response.success){
                    toastr.success("Data have been deleted", 'Success');
                    $('#modal-delete').modal('toggle');
                    table.draw();
                } else{
                    toastr.error(response.message, 'Error');
                }
            },
            error : function(xhr){
                toastr.error(xhr.statusText, 'Error');
            }
        });
    })
});