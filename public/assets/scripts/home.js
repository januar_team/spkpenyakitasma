jQuery(document).ready(function(){
    var table = $('#dataTables').DataTable({
        responsive: true,
        bFilter: true,
        lengthChange: true,
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 3},
            { responsivePriority: 3, targets: 1},
            { responsivePriority: 4, targets: 2},
        ]
    });
});