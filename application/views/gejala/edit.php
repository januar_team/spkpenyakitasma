<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <form method="post">
                <div class="card-header">
                    <div class="card-header-title">Daftar Gejala Penyakit</div>
                </div>
                <div class="card-body">
                    <div class="position-relative row form-group">
                        <label for="code" class="col-sm-2 col-form-label">Kode</label>
                        <div class="col-sm-4">
                            <input name="code" id="code" placeholder="Kode" type="text"
                                   class="form-control <?php echo(form_error('code')? 'is-invalid' : '')?>"
                                   value="<?php echo set_value('code', $symptom->code)?>">
                            <?php if (form_error('code')){ ?>
                                <div class="invalid-feedback">
                                    <?php echo form_error('code')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="position-relative row form-group">
                        <label for="name" class="col-sm-2 col-form-label">Gejala Penyakit</label>
                        <div class="col-sm-6">
                            <input name="name" id="name" placeholder="Gejala Penyakit" type="text"
                                   class="form-control <?php echo(form_error('name')? 'is-invalid' : '')?>"
                                   value="<?php echo set_value('name', $symptom->name)?>">
                            <?php if (form_error('name')){ ?>
                                <div class="invalid-feedback">
                                    <?php echo form_error('name')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="btn-actions-pane-left">
                        <a class="btn btn-light" href="/gejala">Batal</a>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>