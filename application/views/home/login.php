<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo env('APP_NAME')?> - Login</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="ArchitectUI HTML Bootstrap 4 Dashboard Template">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link href="/main.css" rel="stylesheet">
</head>

<body>
<div class="app-container app-theme-white body-tabs-shadow">
    <div class="app-container">
        <div class="h-100 bg-alternate bg-animation">
            <div class="d-flex h-100 justify-content-center align-items-center">
                <div class="mx-auto app-login-box col-md-8">
                    <div class="app-logo-inverse mx-auto mb-3"></div>
                    <div class="modal-dialog w-100 mx-auto">
                        <div class="modal-content">
                            <?php echo form_open('/home/login') ?>
                                <div class="modal-body">
                                    <div class="h5 modal-title text-center">
                                        <h4 class="mt-2">
                                            <div>Welcome back,</div>
                                            <span>Please sign in to your account below.</span>
                                        </h4>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <div class="position-relative form-group">
                                                <input name="username"
                                                       value="<?php echo set_value('username')?>"
                                                       placeholder="Username here..."
                                                       type="text"
                                                       class="form-control <?php echo(form_error('username')? 'is-invalid' : '')?>">
                                                <?php if (form_error('username')){ ?>
                                                <div class="invalid-feedback">
                                                    <?php echo form_error('username')?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="position-relative form-group">
                                                <input name="password"
                                                       value="<?php echo set_value('password')?>"
                                                       placeholder="Password here..."
                                                       type="password"
                                                       class="form-control <?php echo(form_error('password')? 'is-invalid' : '')?>">
                                                <?php if (form_error('password')){ ?>
                                                    <div class="invalid-feedback">
                                                        <?php echo form_error('password')?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="position-relative form-check">
                                        <input name="check" id="exampleCheck"
                                               type="checkbox"
                                               class="form-check-input">
                                        <label
                                                for="exampleCheck" class="form-check-label">Keep me logged in
                                        </label>
                                    </div>-->
                                </div>
                                <div class="modal-footer clearfix">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-primary btn-lg">Login</button>
                                    </div>
                                </div>
                            <?php echo form_close()?>
                        </div>
                    </div>
                    <div class="text-center text-white opacity-8 mt-3">Copyright © <?php echo env('APP_AUTHOR') ?>
                        2019
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/assets/scripts/main.js"></script>
</body>
</html>
