<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo env('APP_NAME') ?></title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"/>
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <link href="/main.css" rel="stylesheet">
    <link href="/assets/css/datatables.min.css" rel="stylesheet">
    <link href="/assets/icheck/skin/_all.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header fixed-footer">
    <div class="app-header header-shadow bg-alternate header-text-light">
        <div class="app-header__logo">
            <div class="logo-src" style="background: none; width: 100%; color: #fff;">
                <h6><?php echo env('APP_NAME')?></h6>
            </div>
            <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                </button>
            </div>
        </div>
        <div class="app-header__menu">
                <span>
                    <button type="button"
                            class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
        </div>
        <div class="app-header__content">
            <div class="app-header-right">
                <div class="header-btn-lg pr-0">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="btn-group">
                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                       class="p-0 btn">
                                        <img width="42" class="rounded-circle" src="assets/images/avatars/2.jpg" alt="">
                                        <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                    </a>
                                    <div tabindex="-1" role="menu" aria-hidden="true"
                                         class="dropdown-menu dropdown-menu-right">
                                        <button type="button" tabindex="0" class="dropdown-item"
                                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Log Out
                                        </button>

                                        <?php echo form_open('/home/logout', ['style' => "display:none", "id" => "logout-form"]) ?>
                                        <?php echo form_close() ?>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-content-left  ml-3 header-user-info">
                                <div class="widget-heading">
                                    <?php echo auth()->getUser()->name ?>
                                </div>
                                <div class="widget-subheading">
                                    <?php echo auth()->getUser()->username ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-main">
        <?php include 'sidebar.php' ?>
        <div class="app-main__outer">

            <div class="app-main__inner">
            <?php echo ($content?:'') ?>
            </div>

            <div class="app-wrapper-footer">
                <div class="app-footer">
                    <div class="app-footer__inner">
                        <div class="app-footer-left">
                            <?php echo env("APP_AUTHOR") ?> © 2019
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript" src="/assets/scripts/main.js"></script>
<script type="text/javascript" src="/assets/scripts/app.js"></script>
<script type="text/javascript" src="/assets/icheck/icheck.min.js"></script>
<script type="application/javascript">
    jQuery(document).ready(function() {
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_square-red',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        jQuery(document).ready(function(){
            $('.btn-reset').click(function (event) {
                $('input.icheck').iCheck('uncheck');
            })
        })
    });
</script>
<?php if (isset($scripts)){
    foreach ($scripts as $script){
        ?>
        <script type="text/javascript" src="<?php echo $script?>"></script>
        <?php
    }
} ?>
</html>
