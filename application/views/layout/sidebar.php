<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                        data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="/" class="<?php echo ($class == 'home' ? 'mm-active' : '')?>">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        Main Dashboard
                    </a>
                </li>
                <?php if (auth()->isRole('superadmin')){ ?>
                <li class="app-sidebar__heading">Master Data</li>
                <li>
                    <a href="/disease" class="<?php echo ($class == 'disease' ? 'mm-active' : '')?>">
                        <i class="metismenu-icon pe-7s-display2"></i>
                        Daftar Penyakit
                    </a>
                </li>
                <li>
                    <a href="/gejala" class="<?php echo ($class == 'gejala' ? 'mm-active' : '')?>">
                        <i class="metismenu-icon pe-7s-display2"></i>
                        Daftar Gejala Penyakit
                    </a>
                </li>
                <li>
                    <a href="/role" class="<?php echo ($class == 'role' ? 'mm-active' : '')?>">
                        <i class="metismenu-icon pe-7s-display2"></i>
                        Basis Aturan
                    </a>
                </li>
                <?php } ?>
                <li class="app-sidebar__heading">Diagnosa</li>
                <li>
                    <a href="/diagnosa" class="<?php echo ($class == 'diagnosa' ? 'mm-active' : '')?>">
                        <i class="metismenu-icon pe-7s-display2"></i>
                        Proses Dianosa Penyakit
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>