<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <form method="post">
            <div class="card-header">
                <div class="card-header-title">Diagnosa Penyakit</div>
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="patient" class="col-sm-6 col-form-label">Nama Pasien</label>
                    <div class="col-sm-4">
                        <input name="patient" id="patient" placeholder="Nama Pasien" type="text" required
                               class="form-control <?php echo(form_error('patient')? 'is-invalid' : '')?>"
                               value="<?php echo set_value('patient')?>">
                        <?php if (form_error('patient')){ ?>
                            <div class="invalid-feedback">
                                <?php echo form_error('patient')?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="patient" class="col-sm-6 col-form-label">Alamat</label>
                    <div class="col-sm-4">
                        <textarea name="address" id="address" placeholder="Nama Pasien" required class="form-control <?php echo(form_error('patient') ? 'is-invalid' : '') ?>"><?php echo set_value('address') ?></textarea>
                        <?php if (form_error('patient')) { ?>
                            <div class="invalid-feedback">
                                <?php echo form_error('patient') ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="patient" class="col-sm-6 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-4">
                        <select name="gender" id="gender" required class="form-control <?php echo(form_error('gender') ? 'is-invalid' : '') ?>">
                            <option value="Laki-laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="patient" class="col-sm-6 col-form-label">No. Handphone</label>
                    <div class="col-sm-4">
                        <input name="phone" id="phone" placeholder="No. Handphone" type="text" required
                               class="form-control <?php echo(form_error('phone') ? 'is-invalid' : '') ?>"
                               value="<?php echo set_value('phone') ?>">
                        <?php if (form_error('phone')) { ?>
                            <div class="invalid-feedback">
                                <?php echo form_error('phone') ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <h5>Gejala Penyakit</h5>
                <!--<table>
                    <tbody>-->
                    <?php
                    foreach ($symptoms as $symptom) {?>
                        <div class="form-group row">
                            <label class="col-sm-6 col-form-label"><?php echo $symptom->name ?></label>
                            <div class="col-sm-4 form-check ">
                                <input type="radio" class="icheck" value="true" name="role-<?php echo $symptom->id ?>">
                                <label class="form-check-label">Ya</label>
                                <input type="radio" class="icheck" value="false" name="role-<?php echo $symptom->id ?>">
                                <label class="form-check-label">Tidak</label>
                            </div>
                        </div>
                    <?php } ?>
                    <!--</tbody>
                </table>-->
            </div>
                <div class="card-footer">
                    <div class="btn-actions-pane-right">
                        <button class="btn btn-ligh btn-reset" type="reset">Clear</button>
                        <button class="btn btn-primary" type="submit">Diagnosa</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
