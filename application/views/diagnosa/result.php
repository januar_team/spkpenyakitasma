<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <form method="post">
                <div class="card-header">
                    <div class="card-header-title">Diagnosa Penyakit - Result</div>
                </div>
                <div class="card-body">
                    <div class="position-relative row form-group">
                        <label for="code" class="col-sm-6 col-form-label">Nama Pasien</label>
                        <div class="col-sm-4">
                            <input name="pasien" id="pasien" placeholder="Nama Pasien" type="text" disabled
                                   class="form-control <?php echo(form_error('pasien') ? 'is-invalid' : '') ?>"
                                   value="<?php echo set_value('pasien', $result->patient) ?>">
                            <?php if (form_error('pasien')) { ?>
                                <div class="invalid-feedback">
                                    <?php echo form_error('pasien') ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="position-relative row form-group">
                        <label for="patient" class="col-sm-6 col-form-label">Alamat</label>
                        <div class="col-sm-4">
                                    <textarea name="address" id="address"  required disabled
                                              class="form-control <?php echo(form_error('patient') ? 'is-invalid' : '') ?>"><?php echo set_value('address', $result->address) ?>
                                    </textarea>
                            <?php if (form_error('patient')) { ?>
                                <div class="invalid-feedback">
                                    <?php echo form_error('patient') ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="patient" class="col-sm-6 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-4">
                            <input name="gender" id="gender" type="text" disabled
                                   class="form-control <?php echo(form_error('gender') ? 'is-invalid' : '') ?>"
                                   value="<?php echo set_value('gender', $result->gender) ?>">
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="patient" class="col-sm-6 col-form-label">No. Handphone</label>
                        <div class="col-sm-4">
                            <input name="phone" id="phone" placeholder="No. Handphone" type="text" required disabled
                                   class="form-control <?php echo(form_error('phone') ? 'is-invalid' : '') ?>"
                                   value="<?php echo set_value('phone', $result->phone) ?>">
                            <?php if (form_error('phone')) { ?>
                                <div class="invalid-feedback">
                                    <?php echo form_error('phone') ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <h5>Gejala Penyakit</h5>
                    <ul>
                        <?php
                        if ($result instanceof \App\Model\Result && count($result->symptom) == 0) {
                            $temp = [];
                            foreach ($result->data as $item) {
                                foreach ($item['symptoms'] as $symptom){
                                    if(!in_array($symptom['id'], $temp))
                                        $temp[] = $symptom['id'];
                                    else
                                        continue;
                                    ?>
                                    <li><p><?php echo $symptom['name'] ?></p></li>
                                    <?php
                                }
                            }
                        } else {
                            foreach ($result->symptom as $symptom) {
                                $item = \App\Model\Symptom::find($symptom['symptom_id']);
                                if (!$item)
                                    continue;

                                ?>
                                <li><p><?php echo $item->name ?></p></li>
                                <?php
                            }
                        } ?>
                    </ul>

                    <h5>Hasil</h5>
                    <table class="table" width="100%">
                        <thead>
                        <tr>
                            <th>Nama Penyakit</th>
                            <th><i>P(Hi)</i></th>
                            <th>Nilai Semesta</th>
                            <th>P(Hi)</th>
                            <th>P(E|Hi)</th>
                            <th>P(Hi|E)</th>
                            <th>Nilai Bayes</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($result->data as &$disease) {
                            if (is_array($disease))
                                $disease = (object)$disease;

                            if (is_array($disease->symptoms))
                                $disease->symptoms = collect($disease->symptoms);

                            ?>
                            <tr>
                                <td class="text-nowrap"><?php echo $disease->name ?></td>
                                <td style="max-width: 180px"><?php echo $disease->symptoms->map(function ($item, $key) {
                                        return (is_array($item) ? $item['pivot']['probability'] : $item->pivot->probability);
                                    }) ?></td>
                                <td><?php echo $disease->semesta ?></td>
                                <td style="max-width: 180px"><?php echo $disease->symptoms->map(function ($item, $key) {
                                        return (is_array($item) ? $item['probability_hypothesis'] : $item->probability_hypothesis);
                                    }) ?></td>
                                <td><?php echo $disease->p_evidence_if_hypothesis ?></td>
                                <td style="max-width: 180px"><?php echo $disease->symptoms->map(function ($item, $key) {
                                        return (is_array($item) ? $item['probability_hypothesis_if_evidence'] : $item->probability_hypothesis_if_evidence);
                                    }) ?></td>
                                <td><?php echo $disease->bayes ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
<?php $temp_result = $result->data->first() ?>
                    <div class="col-lg-7 col-xl-7">
                        <div class="card mb-6 widget-content">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Hasil Diagnosa</div>
                                    <div class="widget-subheading">Nilai Bayes
                                        : <?php echo is_array($temp_result) ? $temp_result['bayes'] : $temp_result->bayes ?></div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="widget-numbers text-success">
                                        <span><?php echo is_array($temp_result) ? $temp_result['name'] : $temp_result->name ?></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="btn-actions-pane-right">
                        <a href="/diagnosa" class="btn btn-light">Kembali</a>
                        <?php if (!($result instanceof \App\Model\Result)) {?>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        <?php } ?>

                        <a href="/diagnosa/print<?php echo (($result instanceof \App\Model\Result) ? '/'. $result->id : '') ?>"
                           target="_blank" class="btn btn-primary">
                            <i class="fa fa-print"></i>
                            Print
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
