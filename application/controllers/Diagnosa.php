<?php


use App\Model\Result;
use App\Model\Symptom;

class Diagnosa extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $response = array();
        if ($this->isPost()) {
            $options = array();
            foreach ($this->input->post() as $key => $opt) {
                if (!preg_match('/^role-*/', $key))
                    continue;

                if ($opt == "false")
                    continue;

                $field = explode("-", $key);
                $options[] = [
                    'symptom_id' => end($field),
                    'value' => $opt,
                ];
            }

            $options = collect($options);

            /** get all probability value */
            $diseases = \App\Model\Disease::with(['symptoms' => function ($q) use ($options) {
                $q->whereIn('id', $options->map(function ($item, $key) {
                    return $item['symptom_id'];
                }));
            }])->get();

            // count semesta value
            foreach ($diseases as $key => $disease) {
                foreach ($disease->symptoms as $symptom) {
                    $diseases[$key]->semesta += $symptom->pivot->probability;
                }
            }

            // calculate P(Hi) and P(E|Hi)
            foreach ($diseases as $d => $disease) {
                foreach ($disease->symptoms as $s => $symptom) {
                    // nilai P(Hi)
                    $disease->symptoms[$s]->probability_hypothesis = round($symptom->pivot->probability / $disease->semesta, 3);

                    // nilai P(E|Hi)
                    $diseases[$d]->p_evidence_if_hypothesis += ($disease->symptoms[$s]->probability_hypothesis * $symptom->pivot->probability);
                }
            }

            //calculate P(Hi|E)
            foreach ($diseases as $d => $disease) {
                foreach ($disease->symptoms as $s => $symptom) {
                    $disease->symptoms[$s]->probability_hypothesis_if_evidence = round(
                        ($symptom->pivot->probability * ($disease->symptoms[$s]->probability_hypothesis * $symptom->pivot->probability))
                        / $disease->p_evidence_if_hypothesis, 4);
                }
            }

            //calculate bayes
            foreach ($diseases as $d => $disease) {
                foreach ($disease->symptoms as $s => $symptom) {
                    // nilai P(Hi)
                    $disease->bayes += ($symptom->pivot->probability * $disease->symptoms[$s]->probability_hypothesis_if_evidence);
                }
            }

            $this->session->set_userdata('result', (object)[
                'patient'   => $this->input->post('patient'),
                'address'   => $this->input->post('address'),
                'gender'    => $this->input->post('gender'),
                'phone'     => $this->input->post('phone'),
                'data'      => $diseases->sortByDesc('bayes'),
                'symptom'   => $options
                ]);
            redirect('/diagnosa/result');
        }

        $response['symptoms'] = Symptom::all();
        $this->view->load($response);
    }

    public function result($id = null){
        $result = ($id) ? Result::find($id) : $this->session->userdata('result');
        if (!$result)
            redirect('/diagnosa');

        $result->data = $result->data->sortByDesc('bayes');

        if ($this->isPost()){
            $data = new Result((array)$result);
            $data->save();

            /*$data->symptoms()->attach($result->symptom->map(function ($item, $key) {
                return $item['symptom_id'];
            })->toArray());
            $data->save();*/

            $this->session->unset_userdata('result');
            redirect('/diagnosa');
        }

        $this->view->load(['result' => $result]);
    }

    public function print($id = null){
        $result = ($id) ? Result::find($id) : $this->session->userdata('result');
        if (!$result)
            redirect('/diagnosa');

        $result->data = $result->data->sortByDesc('bayes');

        $this->view->load(['result' => $result], ['use_template' => false]);
    }
}