<?php


use App\Model\Symptom;

class Gejala extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $response = array();
        if ($this->isPost()){
            $search = '';
            $start = $this->input->post('start');
            $length = $this->input->post('length');

            if( !empty($this->input->post('search')) )
                $search = $this->input->post('search')['value'];
            else
                $search = null;

            $column = [
                "id",
                "code",
                "name",
                "description"
            ];

            $query = Symptom::where(function ($q) use($search){
                $q->where('code', 'LIKE', "%$search%")
                    ->orWhere('name', 'LIKE', "%$search%");
            });

            $total = $query->count();
            $data = $query->orderBy($column[$this->input->post('order')[0]['column']],
                $this->input->post('order')[0]['dir'] )
                ->skip($start)
                ->take($length)
                ->get();

            $response = [
                'data' => $data,
                'draw' => intval($this->input->post('draw')),
                'recordsTotal' => $total,
                'recordsFiltered' => $total
            ];

            return $this->json($response);
        }

        $scripts = [
            '/assets/scripts/symptom.js'
        ];
        $response['scripts'] = $scripts;
        $this->view->load($response);
    }

    public function add(){
        $response = array();
        if ($this->isPost()){
            $this->form_validation->set_rules('code', 'Kode',
                'required|alpha_numeric|is_unique[symptoms.code]',
                [
                    'is_unique' => 'This %s already exists.'
                ]);
            $this->form_validation->set_rules('name', 'Gejala', 'required');
            if ($this->form_validation->run()){
                $symptom = new Symptom($this->input->post());
                $symptom->save();
                redirect('/gejala');
            }
        }

        $this->view->load($response);
    }

    public function edit($id){
        $response = array();
        $symptom = Symptom::find($id);
        if (!$symptom){
            redirect('/gejala');
        }

        if ($this->isPost()){
            $this->form_validation->set_rules('code', 'Kode',
                "required|alpha_numeric|is_unique[symptoms.code.id=$symptom->id]",
                [
                    'is_unique' => 'This %s already exists.'
                ]);
            $this->form_validation->set_rules('name', 'Nama Penyakit', 'required');
            if ($this->form_validation->run()){
                $symptom->fill($this->input->post());
                $symptom->save();
                redirect('gejala');
            }
        }

        $response['symptom'] = $symptom;
        $this->view->load($response);
    }

    public function delete($id){
        $response = [
            'success' => false,
            'message' => ''
        ];

        if ($this->isPost()){
            $symptom = Symptom::find($id);
            if (!$symptom){
                $response['message'] = 'Data not found';
            }else{
                $symptom->delete();
                $response['success'] = true;
            }
        }else{
            $response['message'] = 'Method not allowed!';
        }

        $this->json($response);
    }
}