<?php
if ( ! function_exists('auth'))
{
    /**
     * Get Auth object for retrive authenticate user
     *
     * @return	Auth
     */
    function auth()
    {
        $CI =& get_instance();

        return $CI->auth;
    }
}